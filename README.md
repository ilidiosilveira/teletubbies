# Documentação

Segue a documentação do [Markdown](https://www.inteligenciaurbana.org/2021/04/markdown-parte1.html) para auxilio de todo esse processo.

Esse documento tem o propósito de deixar claro as ferramentas que serão utilizadas na disciplina projeto de software na faculdade IBMEC a pedido do professor **LINDO**.

Muito provavelmente o conteúdo vai estar mesclado em português e inglês, porém vou sempre priorizar o inglês.
Porque quero aprimorar cada dia mais minha proeficiência na lingua mais importante do mundo e aconselho que você também foque no inglês. 

Não sabe? Comece hoje a praticar usando o Tradutor e tenho certeza que vai chegar lá.

---

# Ferramentas que serão utilizadas

### HTML - HyperText Markup Language

Okay, so this is the only bit of mandatory theory. In order to begin to write HTML, it helps if you know what you are writing.

HTML is the language in which most websites are written. HTML is used to create pages and make them functional.

| Vantagens  | Desvantagens   |
| -------- | -------- |
| Fácil Customização   | Não printa documentos ou códigos   |
| Noob-friendly   | Não possui toolbar ou dashboard disponível    |

[**Saiba mais...**](https://html.com/)


### CSS - Cascading Style Sheets

CSS (Cascading Style Sheets) is used to style and layout web pages — for example, to alter the font, color, size, and spacing of your content, split it into multiple columns, or add animations and other decorative features. This module provides a gentle beginning to your path towards CSS mastery with the basics of how it works, what the syntax looks like, and how you can start using it to add styling to HTML.

[**Saiba mais...**](https://developer.mozilla.org/en-US/docs/Web/CSS)

### JavaScript

JavaScript (often shortened to JS) is a lightweight, interpreted, object-oriented language with first-class functions, and is best known as the scripting language for Web pages, but it's used in many non-browser environments as well. It is a prototype-based, multi-paradigm scripting language that is dynamic, and supports object-oriented, imperative, and functional programming styles.

JavaScript runs on the client side of the web, which can be used to design / program how the web pages behave on the occurrence of an event. JavaScript is an easy to learn and also powerful scripting language, widely used for controlling web page behavior.

Contrary to popular misconception, JavaScript is not "Interpreted Java". In a nutshell, JavaScript is a dynamic scripting language supporting prototype based object construction. The basic syntax is intentionally similar to both Java and C++ to reduce the number of new concepts required to learn the language. Language constructs, such as `if` statements, `for` and `while` `loops`, and `switch` and `try` ... `catch` blocks function the same as in these languages (or nearly so).

[**Saiba mais...**](https://www.javascript.com/)

### React


React is a JavaScript library for building user interfaces.

React has been designed from the start for gradual adoption, and you can use as little or as much React as you need. Whether you want to get a taste of React, add some interactivity to a simple HTML page, or start a complex React-powered app, the links in this section will help you get started.

[**Saiba mais...**](https://reactjs.org/docs/getting-started.html)

### Scrum

Scrum is a framework within which people can address complex adaptive problems, while productively and creatively delivering products of the highest possible value.

[**Video Informativo**](https://www.youtube.com/watch?v=2Vt7Ik8Ublw&ab_channel=OrganizeAgile)

#### Alguns conceitos importantes do SCRUM

**Sprints**: é o nome dado para os ciclos de cada projeto. Em geral são ciclos mensais e são determinados para que as tarefas sejam realizadas.

**Product Backlog**: é o nome dado para o conjunto de objetivos de um projeto. No caso de um projeto de desenvolvimento de software (para o qual o Scrum foi pensado inicialmente), é o nome dado ao pacote de funcionalidades a serem desenvolvidas em um projeto.

**Sprint Planning Meeting**: são reuniões periódicas que acontecem no início de cada sprint, ou ciclo, para planejar e priorizar os itens do Product Backlog que serão desenvolvidos naquele período.
Sprint Backlog: é como se chamam as tarefas específicas que serão realizadas e desenvolvidas em cada ciclo, ou sprint.

**Daily Scrum**: essa é uma reunião diária para acompanhamento do projeto. A ideia é que toda a equipe se reúna diariamente para discutir as atividades desenvolvidas, disseminar conhecimento, identificar impedimentos e priorizar o trabalho daquele dia. Um ponto interessante é que o Scrum propõe que estas reuniões sejam realizadas com os participantes em pé, exatamente para serem rápidas e objetivas.

**Sprint Review Meeting**: essa é a reunião que acontece ao final de cada sprint para que a equipe apresente o que foi realizado e os resultados do trabalho daquele ciclo. A ideia é que depois dessa etapa, todos sigam para o próximo ciclo.

[**Ainda não entendeu mula?**](https://www.youtube.com/watch?v=9TycLR0TqFA&ab_channel=Uzility)


---
#### Atualizado no dia 18/08/2021 as 9:00





